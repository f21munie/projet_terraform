stages:          # List of stages for jobs, and their order of execution
  - init
  - plan
  - apply
  - destroy
  - on_failure

image:
    name: "hashicorp/terraform:latest"
    entrypoint: 
      - /usr/bin/env
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

init_step:
  stage: "init"
  only:
    - master
  script:
    - source $OPENRC
    - env | grep OS_
    - terraform init 
  artifacts:
    name: "init_step:${CI_JOB_ID}"
    when: "on_success"
    expire_in: "6h"
    paths:
      - ./.terraform/
      - ./.terraform.lock.hcl/

plan_step:
  stage: plan
  only:
    - master
  script:
    - terraform plan -out=tfplan
  artifacts:
    name: "plan_step:${CI_JOB_ID}"
    when: "on_success"
    expire_in: "6h"
    paths:
      - ./tfplan
  dependencies:
    - init_step
  

apply_step:
  stage: apply
  only:
    - master
  script:
    - terraform apply -input=false tfplan
  dependencies:
    - init_step
    - plan_step
  artifacts:
    name: "apply_step:${CI_JOB_ID}"
    when: always
    expire_in: "6h"
    paths:
      - ./terraform.tfstate
      - .ssh/
      - ./ip_files/
      - ./hosts.ini
      - ./ssh.cfg
  
destroy_step:
  stage: destroy
  only:
    - master
  script:
    - terraform destroy -auto-approve
  when: manual
  dependencies:
    - init_step
    - plan_step
    - apply_step

destroy_on_failure:
  stage: on_failure
  only:
    - master
  script:
    - terraform destroy -auto-approve
  when: on_failure
  dependencies:
    - init_step
    - plan_step
    - apply_step